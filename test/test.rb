# Small Ruby script to help test gem features
#
# Run with `bundle exec jruby --debug test/test.rb`

require 'pry'
require 'pry-debugger-jruby'

def some_method
  binding.pry          # Execution will stop here.
  Hello.new.potato
  puts 'Hello, World!'
end

class Hello
  def potato
    puts "potato1"
    puts "potato2"
  end
end

some_method
